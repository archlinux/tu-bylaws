# Arch Package Maintainer Bylaws

This document (`package-maintainer-bylaws.adoc`) describes the bylaws which govern the Arch Linux
Package Maintainers.

Instructions for updating:

- follow the procedure outlined in the bylaws for proposing an update
- if accepted, merge the related merge request to the master branch and tag the commit
  "proposal-$num" to correspond with the vote ID at
  https://aur.archlinux.org/package-maintainer/
